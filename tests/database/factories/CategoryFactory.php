<?php

namespace KDA\Tests\Database\Factories;

use KDA\Tests\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    protected $model = Category::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(4),
        ];
    }
}
