<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Eloquent\NestedSet\Traits\NestedCreate;
use KDA\Eloquent\NestedSet\Traits\NestedDelete;
use KDA\Eloquent\NestedSet\Traits\NestedParent;
use KDA\Eloquent\NestedSet\Traits\NestedRelationships;

class Category extends Model 
{
   
    use HasFactory;
    use NestedParent;
    use NestedCreate;
    use NestedDelete;
    use NestedRelationships;

    protected $fillable = [
        'title',
        'parent_id',
        'lft',
        'rgt',
        'depth'
    ];

  
    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\CategoryFactory::new();
    }
}
