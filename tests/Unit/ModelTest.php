<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Tests\Models\Category;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;


  /** @test */
  function basic_nesting()
  {
    $c1 = Category::factory()->create([]);

    $c2 = Category::factory()->create([]);


    $c3 = Category::factory()->create(['parent_id'=>$c2->id]);

    $this->assertTrue($c2->fresh()->rgt > $c3->fresh()->rgt);
  }


  /** @test */
  function more_nesting()
  {
    $c1 = Category::factory()->create([]);

    $c2 = Category::factory()->create([]);


    $c3 = Category::factory()->create(['parent_id'=>$c2->id]);
    $c4 = Category::factory()->create(['parent_id'=>$c2->id]);
    $c5 = Category::factory()->create([]);

   //dump(Category::all()->toArray());
  }


  /** @test */
  function delete_nested()
  {
    $c1 = Category::factory()->create(['title'=>'cat 1']);

    $c2 = Category::factory()->create(['title'=>'cat 2']);


    $c3 = Category::factory()->create(['parent_id'=>$c2->id]);
    $c4 = Category::factory()->create(['parent_id'=>$c2->id]);
    $c5 = Category::factory()->create(['title'=>'cat 5']);
    dump(Category::all()->toArray());

   $c2->fresh()->delete();

   dump(Category::all()->toArray());

  }



  
}