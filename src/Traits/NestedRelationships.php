<?php

namespace KDA\Eloquent\NestedSet\Traits;

use DB;

/*
implementation of 
http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
*/

trait NestedRelationships
{

    public function parent()
    {
        return $this->belongsTo(static::class, $this->getParentKeyName());
    }

    public function children()
    {
        return $this->hasMany(static::class, $this->getParentKeyName());
    }
}
