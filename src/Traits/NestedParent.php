<?php

namespace KDA\Eloquent\NestedSet\Traits;

/*
implementation of 
http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
*/

trait NestedParent
{

    public function getParentKeyName(){
        return $this->parentAttributeName ?? 'parent_id';
    }

}
