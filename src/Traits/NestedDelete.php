<?php

namespace KDA\Eloquent\NestedSet\Traits;
use DB;
/*
implementation of 
http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
*/

trait NestedDelete
{

    use Introspect;
    public static function bootNestedDelete(): void
    {
        static::deleted(function ($model) {
           
            $model->applyDeleteNestedSetAttributes();
            
        });
    }


    public function applyDeleteNestedSetAttributes(){
        $table_name = self::tableName();
        $left = $this->lft;
        $right = $this->rgt;
        $width = $right - $left +1;
        static::whereBetween('lft',[$left,$right])->delete();
        DB::statement('update ' . $table_name . ' set rgt = rgt - '.$width.' where rgt > ' . $right);
        DB::statement('update ' . $table_name . ' set lft = lft - '.$width.' where lft > ' . $right);
    }
}
