<?php

namespace KDA\Eloquent\NestedSet\Traits;

use DB;
/*
implementation of
http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
*/

trait NestedCreate
{
    use Introspect;

    public static function bootNestedCreate(): void
    {
        static::creating(function ($model) {
            $model->applyCreateNestedSetAttributes();
        });
    }

    public function applyCreateNestedSetAttributes()
    {
        $baseQuery = static::when(blank($this->parent_id), function ($q) {
            return $q->whereNull('parent_id');
        })->when(!blank($this->parent_id), function ($q) {
            return $q->where('parent_id', $this->parent_id);
        });

        $table_name = self::tableName();
        $siblings = $baseQuery->count();
        //$right = $baseQuery->max('rgt');

        if ($siblings > 0 || blank($this->parent_id)) {
            $right = $baseQuery->max('rgt') ?? 0;
            $this->lft = $right + 1;
            $this->rgt = $right + 2;
            DB::statement('update ' . $table_name . ' set rgt = rgt+2 where rgt > ' . $right);
            DB::statement('update ' . $table_name . ' set lft = lft+2 where lft > ' . $right);
        } else {
            //  $right= static::where('parent_id',$this->parent_id)->max('rgt') ?? 0;
            $left = static::where('id', $this->parent_id)->max('lft') ?? 0;
            $this->lft = $left + 1;
            $this->rgt = $left + 2;
            DB::statement('update ' . $table_name . ' set rgt = rgt+2 where rgt > ' . $left);
            DB::statement('update ' . $table_name . ' set lft = lft+2 where lft > ' . $left);
        }
    }
}
