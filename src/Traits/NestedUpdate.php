<?php

namespace KDA\Eloquent\NestedSet\Traits;

/*
implementation of 
http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
*/

trait NestedUpdate
{


    public static function bootNestedUpdate(): void
    {
        static::updating(function ($model) {
           
            $model->applyUpdateNestedSetAttributes();
            
        });
    }


    public function applyUpdateNestedSetAttributes(){
        
    }
}
