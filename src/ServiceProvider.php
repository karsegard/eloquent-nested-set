<?php
namespace KDA\Eloquent\NestedSet;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\ProvidesBlueprint;
    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
        $this->blueprints =[
            'nestable'=> function($left="lft",$right="rgt",$depth="depth"){
                $this->unsignedInteger($left)->nullable()->default(0);
                $this->unsignedInteger($right)->nullable()->default(0);
                $this->unsignedInteger($depth)->nullable()->default(0);
            },
            'nestable_parent'=> function($parent_id_name='parent_id',$constraint=NULL,$onDelete='set null'){
                $table = $constraint ?? $this->table;
                $this->foreignId($parent_id_name)->nullable()->constrained($table)->onDelete($onDelete);
            }
        ];
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
        
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
